package cz.cvut.fel.omo.utils;

/**
 * This class represents constants.
 *
 * @author Oľga Ostashchuk, Tereza Lodrová, Michal Pechník
 * @version 1.0
 */
public class Constants {

    /**
     * Default price for water per liter.
     */
    public static final double WATER_PRICE_PER_LITER = 0.09;
    /**
     * Default price for electricity per kWh.
     */
    public static final double ELECTRICITY_PRICE_PER_kWh = 1.9;


    /**
     * Default manual for device.
     */
    public static final String RANDOM_STRING_FOR_DEVICE_MANUAL = "Lorem ipsum dolor sit amet, consectetur " +
            "adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim " +
            "veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. " +
            "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore" +
            " eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui " +
            "officia deserunt mollit anim id est laborum.";
}
