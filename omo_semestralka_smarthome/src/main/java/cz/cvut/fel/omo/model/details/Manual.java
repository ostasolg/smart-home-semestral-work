package cz.cvut.fel.omo.model.details;

/**
 * This class represents manual.
 *
 * @author Oľga Ostashchuk, Tereza Lodrová, Michal Pechník
 * @version 1.0
 */
public class Manual {

    private String manualText;

    /**
     * Returns manual.
     * @return manualText String
     */
    public String getManual() {
        return manualText;
    }

    /**
     * Sets manual.
     * @param  manual String
     */
    public void setManual(String manual) {
        this.manualText = manual;
    }
}
