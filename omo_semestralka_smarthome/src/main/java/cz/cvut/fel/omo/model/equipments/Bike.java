package cz.cvut.fel.omo.model.equipments;


/**
 * This class represents bike.
 *
 * @author Oľga Ostashchuk, Tereza Lodrová, Michal Pechník
 * @version 1.0
 */
public class Bike extends Equipment {


    public Bike() {
        super();
        this.name="Bike";
    }
}
