package cz.cvut.fel.omo.model.equipments;

/**
 * This class represents ski.
 *
 * @author Oľga Ostashchuk, Tereza Lodrová, Michal Pechník
 * @version 1.0
 */
public class Ski extends Equipment {


    public Ski() {
        super();
        this.name="Ski";
    }
}
